@extends('layout.master')    
 @section('title')
    List Karyawan
 @endsection
 @section('content')  
 <div class="card">
        
    <div class="card-body p-0">
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 1%">
                      No
                  </th>
                  <th style="width: 20%">
                      Nama Karyawan
                  </th>
                  <th style="width: 30%">
                      Jumlah Akta Sedang Dikerjakan
                  </th>
                  <th style="width: 30%">
                      Jumlah Akta Selesai Dikerjakan
                  </th>
                  <th style="width: 20%">
                      Action
                  </th>
              </tr>
          </thead>
          <tbody>
          @forelse ($karyawan as $key => $karyawan)  
              <tr>
                  <td>
                  {{$key+1}}
                  </td>
                   <td>
                   {{$karyawan->nama_karyawan}}
                  </td>
                  <td>
                      
                  </td>
                  <td>
                      
                  </td>
                  <td>
                      <a class="btn btn-primary btn-sm" href="/detailKaryawan/{{$karyawan->id}}">
                          <i class="fas fa-folder">
                          </i>
                          View
                      </a>
                      
                  </td>
              </tr>
              @empty
                  <tr>
                  <td colspan="4" align="center">No Data</td>
                  </tr>
                 @endforelse
             
          </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
