 @extends('layout.master')    
 @section('title')
    List Klien
 @endsection
 @section('content')   
 <div class="card">
        <div class="card-body p-0">
          <table class="table table-striped projects">
              <thead>
                  <tr>
                      <th style="width: 1%">
                          No
                      </th>
                      <th style="width: 20%">
                          Nama Klien
                      </th>
                      <th style="width: 30%">
                          Jumlah Order
                      </th>
                      <th style="width: 20%">
                          Action
                      </th>
                  </tr>
              </thead>
              <tbody>
              @forelse ($klien as $key => $klien)
                  <tr>
                      <td>{{$key+1}}</td>
                       <td>
                       {{$klien->nama_klien}}
                      </td>
                      <td>
                          
                      </td>
                      <td>
                      <form action="/hapusKlien/{{$klien->id}}" method="POST">
                @csrf
                @method('DELETE')
                          <a class="btn btn-primary btn-sm" href="/detailKlien/{{$klien->id}}">
                              <i class="fas fa-folder">
                              </i>
                              View
                          </a>
                          
                          <a>
                          
                    <button type="submit" class="btn btn-danger btn-sm">
                    <i class="fas fa-trash"></i> Delete
                    </a>
                </form>
              
                          
                      </td>
                  </tr>
                  @empty
                  <tr>
                  <td colspan="4" align="center">No Data</td>
                  </tr>
                 @endforelse
              </tbody>
          </table>
        </div>
</div>
        <!-- /.card-body -->
      
@endsection   
