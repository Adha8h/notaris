@extends('layout.master')    
 @section('title')
    Detail Dokumen Akta
 @endsection
 @section('content')  
 <div class="container-fluid">
  <div class="row">
    <div class="col-md-6">

      <!-- Profile Image -->
     
      <!-- /.card -->

      <!-- About Me Box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Detail Dokumen Akta</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <strong><i class="fas fa-book mr-1"></i> Nama Akta</strong>

          <p class="text-muted">
            
          {{$data->judul_akta}}
          </p>
         <hr>
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Nama Klien</strong>

          <p class="text-muted">{{$data->nama_klien}}</p>

          <hr>
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Nama Perusahaan</strong>

          <p class="text-muted">{{$data->nama_perusahaan}}</p>

          <hr>

          <strong><i class="fas fa-map-marker-alt mr-1"></i> Jenis Akta</strong>

          <p class="text-muted">{{$data->jenis_akta}}</p>

          <hr>
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Penanggung Jawab</strong>

          <p class="text-muted">{{$data->nama_karyawan}}</p>

          <hr>
           <strong><i class="fas fa-map-marker-alt mr-1"></i> Tanggal Mulai</strong>

          <p class="text-muted">{{$data->tanggal_mulai}}</p>

          <hr>
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Tanggal Tandatangan</strong>

          <p class="text-muted">-</p>

          <hr>
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Tanggal Selesai</strong>

          <p class="text-muted">{{$data->tanggal_selesai}}</p>

          <hr>
          
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    
    <div class="col-md-6">

      <!-- Profile Image -->
     
      <!-- /.card -->

      <!-- About Me Box -->
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Detail Dokumen Akta</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Minuta Akta</strong>

          <p class="text-muted"><a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i> Minuta Akta.pdf</a></p>
          <a href="#" class="btn btn-sm btn-primary">Add files</a>
          <a href="#" class="btn btn-sm btn-primary">Setujui</a>

          <hr>
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Salinan Akta</strong>

          <p class="text-muted"><a href="" class="btn-link text-secondary"><i class="far fa-fw fa-file-pdf"></i> Salinan Akta.pdf</a></p>
          <a href="#" class="btn btn-sm btn-primary">Add files</a>
          <a href="#" class="btn btn-sm btn-primary">Setujui</a>
          <a href="#" class="btn btn-sm btn-primary">Revisi</a>
          <hr>
          <strong><i class="fas fa-map-marker-alt mr-1"></i> Status Dokumen</strong>

          <p class="text-muted">Lorem Ipsum</p>
          
          <hr>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    
    <!-- /.col -->
    
  </div>
  <!-- /.row -->
  <div class="row">
  <div class="col-md">
    <div class="card card-primary">
    <div class="card-header">
    <h3 class="card-title">Timeline Pengerjaan</h3>
  </div>
  </div>
  <div class="card-body p-0">

  <section class="content">
<div class="container-fluid">

  <!-- Timelime example  -->
  <div class="row">
    <div class="col-md-12">
      <!-- The time line -->
      <div class="timeline">
        <!-- timeline time label -->
       
        <!-- /.timeline-label -->
        <!-- timeline item -->
        <div>
          
          <div class="timeline-item">
            <span class="time"><i class="fas fa-clock"></i> 12:05</span>
            <h3 class="timeline-header"><a href="#">Lorem Ipsum</a> Lorem Ipsum</h3>

            <div class="timeline-body">
              Lorem Ipsum
            </div>
            <div class="timeline-footer">
              
            </div>
          </div>
        </div>
        <!-- END timeline item -->
        <!-- timeline item -->
       
        <!-- END timeline item -->
        <!-- timeline item -->
        <div>
          
          <div class="timeline-item">
            <span class="time"><i class="fas fa-clock"></i> 27 mins ago</span>
            <h3 class="timeline-header"><a href="#">Lorem Ipsum</a> Lorem Ipsum</h3>
            <div class="timeline-body">
              Lorem Ipsum
            </div>
            <div class="timeline-footer">
            
            </div>
          </div>
        </div>
        <!-- END timeline item -->
        <!-- timeline time label -->
        
      </div>
    </div>
    <!-- /.col -->
  </div>
</div>
@endsection
