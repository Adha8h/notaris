@extends('layout.master')    
 @section('title')
    Detail Klien
 @endsection
 @section('content')  
 <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md">

          <!-- Profile Image -->
          <div class="card card-primary card-outline">
            <div class="card-body box-profile">
              <div class="text-center">
                <img class="profile-user-img img-fluid img-circle"
                     src="{{asset('admin/dist/img/user4-128x128.jpg')}}"
                     alt="User profile picture">
              </div>

              <h3 class="profile-username text-center">{{$klien->nama_klien}}</h3>

              
       
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->

          <!-- About Me Box -->
          <div class="card card-primary">
            <div class="card-header">
              <h3 class="card-title">Detail Klien</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <strong><i class="fas fa-book mr-1"></i> Nama Perusahaan</strong>

              <p class="text-muted">
              {{$klien->nama_perusahaan}}
              </p>

              <hr>

              <strong><i class="fas fa-map-marker-alt mr-1"></i> NO HP</strong>

              <p class="text-muted">{{$klien->nomor_hp}}</p>

              <hr>
              <strong><i class="fas fa-map-marker-alt mr-1"></i> Email</strong>

              <p class="text-muted">{{$klien->email}}</p>

              <hr>

              <strong><i class="fas fa-map-marker-alt mr-1"></i> Alamat</strong>

              <p class="text-muted">{{$klien->alamat}}</p>

              <hr>

              
            </div>
            <!-- /.card-body -->
            
            @csrf
            <div class="card-footer">
            <a href="/editKlien/{{$klien->id}}" class="btn btn-warning" >Edit</a>
                </div>
              
          </div>
          <!-- /.card -->
        </div>
        
        <!-- /.col -->
        
      </div>
      <!-- /.row -->
      
        <div class="card card-primary">
        <div class="card-header">
        <h3 class="card-title">Riwayat Order</h3>
</div>
      <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th style="width: 1%">
                        No
                    </th>
                    <th style="width: 35%">
                        Nama Akta
                    </th>
                    <th style="width: 10%">
                        Jenis Akta
                    </th>
                    <th style="width: 20%">
                        Tahun Pembuatan
                    </th>
                    <th>
                       Action
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                       
                    </td>
                     <td>
                        
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        
                    </td>
                    <td>
                        <a class="btn btn-primary btn-sm" href="/detailKlien">
                            <i class="fas fa-folder">
                            </i>
                            View
                        </a>
                        
                    </td>
                </tr>
               
               
            </tbody>
        </table>
      </div>
      <!-- /.card-body -->
    </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
    </div><!-- /.container-fluid -->
  </section>
@endsection
