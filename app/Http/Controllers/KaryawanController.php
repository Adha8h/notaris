<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Profile;

class KaryawanController extends Controller
{
    public function listKaryawan()
    {
        $karyawan = User::all();
        return view('karyawan.listKaryawan', compact('karyawan'));
    }
    public function detailKaryawan($id)
    {
        $karyawan = User::where('id',$id)->first();
        $profileKaryawan = Profile::where('user_id',$id)->first();
        //if($profileKaryawan==null){
            return view('karyawan.detailKaryawanBaru', compact('karyawan','profileKaryawan'));
        //}
        //else{
          //  return view('karyawan.detailKaryawan', compact('karyawan','profileKaryawan'));
        //}
        
    }
    public function profile()
    {
        $karyawan = User::where('id', auth()->user()->id)->first();
        $profileKaryawan = Profile::where('user_id', auth()->user()->id)->first();
        
            return view('karyawan.profileBaru', compact('karyawan','profileKaryawan'));
       
    }
    public function editProfile()
    {
        $karyawan = User::where('id', auth()->user()->id)->first();
        $profileKaryawan = Profile::where('user_id', auth()->user()->id)->first();
        
            return view('karyawan.editProfile', compact('karyawan','profileKaryawan'));
       
    }
    public function updateProfile(Request $request)
    {
        //
        $karyawan = User::where('id', auth()->user()->id)
            ->updateOrCreate([
                'username' => $request->username,
                'email_karyawan' => $request->email_karyawan
                
            ]);
        
        $profileKaryawan = Profile::where('user_id', auth()->user()->id)
            ->updateOrCreate([
                'user_id' => auth()->user()->id],
                ['alamat' => $request->alamat,
                'no_hp' => $request->no_hp
                
            ]);
        return redirect('/profile');
    }
}
