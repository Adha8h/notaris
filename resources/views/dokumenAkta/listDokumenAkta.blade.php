@extends('layout.master')    
 @section('title')
    List Dokumen Akta
 @endsection
 @section('content')  
 <div class="card">
        
    <div class="card-body p-0">
      <table class="table table-striped projects">
          <thead>
              <tr>
                  <th style="width: 1%">
                      No
                  </th>
                  <th style="width: 20%">
                      Nama Akta
                  </th>
                  <th style="width: 20%">
                      Nama Klien
                  </th>
                  <th style="width: 20%">
                      Penanggung Jawab
                  </th>
                  <th style="width: 20%">
                      Tahun Pembuatan
                  </th>
                  <th style="width: 20%">
                      Status
                  </th>
                  <th style="width: 20%">
                      Action
                  </th>
              </tr>
          </thead>
          <tbody>
          @forelse ($data as $key => $data)  
          
              <tr>
                  <td>
                  {{$key+1}}
                  </td>
                   <td>
                   {{$data->judul_akta}}
                  </td>
                  <td>
                 
                     {{$data->nama_klien}}
                     
                  </td>
                  <td>
                      {{$data->nama_karyawan}}
                  </td>
                  <td>
                     {{$data->tanggal_mulai}}
                  </td>
                  
                  <td>
                  
                  </td>
                  <td>
                      <a class="btn btn-primary btn-sm" href="/detailAkta/{{$data->id}}">
                          <i class="fas fa-folder">
                          </i>
                          View
                      </a>
                      
                  </td>
              </tr>
              @empty
                  <tr>
                  <td colspan="4" align="center">No Data</td>
                  </tr>
                  @endforelse
             
          </tbody>
      </table>
    </div>
    <!-- /.card-body -->
  </div>
@endsection
