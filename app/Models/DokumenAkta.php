<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DokumenAkta extends Model
{
    use HasFactory;
    protected $table = 'dokumen_akta';
    protected $fillable = [
        'nomor_akta',
        'judul_akta',
        'jenis_akta',
        'tanggal_mulai',
        'tanggal_tandatangan',
        'tanggal_selesai'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
