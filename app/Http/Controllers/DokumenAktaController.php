<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Klien;
use App\Models\DokumenAkta;

class DokumenAktaController extends Controller
{
    //public function listAkta()
    //{
     //   $dokumenAkta = DokumenAkta::all();
      //  $klien = Klien::all();
      //  $karyawan = User::all();
       // return view('dokumenAkta.listDokumenAkta',compact('klien','dokumenAkta','karyawan'));
    //}
    public function listAkta()
    {
        $data = DokumenAkta::join('users', 'users.id','=','dokumen_akta.user_id')
        ->join('klien','klien.id','=','dokumen_akta.klien_id')
        ->get(['dokumen_akta.id','dokumen_akta.judul_akta','klien.nama_klien','users.nama_karyawan','dokumen_akta.tanggal_mulai']);
        
        return view('dokumenAkta.listDokumenAkta',compact('data'));
    }
    public function tambahAkta()
    {
        $klien = Klien::all();
        return view('dokumenAkta.tambahDokumenAkta', compact('klien'));
    }
    public function store(request $request)
    {
        $dokumenAkta = new DokumenAkta;
        $dokumenAkta->klien_id = $request -> klien_id;
        $dokumenAkta->judul_akta = $request -> judul_akta;
        $dokumenAkta->jenis_akta = $request -> jenis_akta;
        $dokumenAkta->nomor_akta = $request -> nomor_akta;
        $dokumenAkta->tanggal_mulai = $request -> tanggal_mulai;
        $dokumenAkta->tanggal_selesai = $request -> tanggal_selesai;
        $dokumenAkta->user_id = auth()->user()->id;
        $dokumenAkta->save();

        return redirect('/listAkta')->with('success', 'Pertanyaan berhasil disimpan');
    }
    public function detailAkta($id)
    {
        $data = DokumenAkta::join('users', 'users.id','=','dokumen_akta.user_id')
        ->join('klien','klien.id','=','dokumen_akta.klien_id')
        ->where('dokumen_akta.id','=', $id)
        ->get()->first();
        return view('dokumenAkta.detailAkta',compact('data'));
    }
    public function cariAkta()
    {
        return view('dokumenAkta.cariDokumenAkta');
    }
    public function hasilPencarian()
    {
        return view('dokumenAkta.hasilPencarian');
    }
}
