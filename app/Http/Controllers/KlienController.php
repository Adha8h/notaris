<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Klien;

class KlienController extends Controller
{
    public function listKlien()
    {
        //
        $klien = Klien::all();
        return view('klien.listKlien', compact('klien'));
    }
    public function tambahKlien()
    {
        return view('klien.tambahKlien');
    }
    public function store(Request $request)
    {
        $klien = new Klien;
        $klien->nama_klien = $request -> nama_klien;
        $klien->nama_perusahaan = $request -> nama_perusahaan;
        $klien->nomor_hp = $request -> nomor_hp;
        $klien->email = $request -> email;
        $klien->alamat = $request -> alamat;
        $klien->save();

        return redirect('/listKlien')->with('success', 'Pertanyaan berhasil disimpan');
    }
    public function show($id)
    {
        $klien = Klien::where('id',$id)->first();
        return view('klien.detailKlien', compact('klien'));
    }
    public function edit($id)
    {
        $klien = Klien::where('id',$id)->first();
        return view('klien.editKlien', compact('klien'));
    }
    public function update(Request $request, $id)
    {
        //
        $klien = Klien::where('id', $id)
            ->update([
                'nama_klien' => $request->nama_klien,
                'nama_perusahaan' => $request->nama_perusahaan,
                'nomor_hp' => $request->nomor_hp,
                'email' => $request->email,
                'alamat' => $request->alamat
            ]);
        return redirect('detailKlien/'.$id);
    }
    public function destroy($id)
    {
        //
        $klien = Klien::find($id);
        $klien->delete();
        return redirect('/listKlien')->with('success', 'Pertanyaan Berhasil Dihapus');
    }
}
